#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"

//G4
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"



#include "G4RunManagerFactory.hh"
#include "G4SteppingVerbose.hh"
#include "G4UImanager.hh"
#include "QBBC.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "Randomize.hh"


int main(int argc, char** argv){

  bool qtVis = true;

  G4UIExecutive* uiExe = 0;
  if(qtVis)
    uiExe = new G4UIExecutive(1, argv);

  //
  if(argc!=2){
    G4cout<<" ERROR of the input parameters !!! "<<G4endl
          <<"      [0] - vis.mac"<<G4endl;
    return 0;    
  }
  else{
    G4cout<<"  Input parameters         "<<G4endl
          <<"     mac file              "<<argv[1]<<G4endl;
  }
  //
  G4RunManager *run_manager = new G4RunManager;
  
  DetectorConstruction *detector_construction = new DetectorConstruction();
  
  run_manager->SetUserInitialization(detector_construction);
  
  PhysicsList *physics_list = new PhysicsList;
  run_manager->SetUserInitialization(physics_list);
  
  PrimaryGeneratorAction *primary_generator = new PrimaryGeneratorAction;
  run_manager->SetUserAction(primary_generator);
  
  run_manager->Initialize();
  
  G4VisManager *vis_manager = new G4VisExecutive;
  vis_manager->Initialize();

  G4UImanager *ui_manager = G4UImanager::GetUIpointer();

  if(qtVis){
    ui_manager->ApplyCommand("/control/execute init_vis.mac");
    uiExe->SessionStart();
    delete uiExe;
  }
  else{
    G4UIExecutive *ui = new G4UIExecutive(argc, argv);
    ui_manager->ApplyCommand(G4String("/control/execute ") + G4String(argv[1]));
  }

  delete ui_manager;
  
  return 0;
}


