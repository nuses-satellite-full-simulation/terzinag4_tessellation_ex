#!/bin/bash

function printHelp {
    echo " --> ERROR in input arguments "
    echo " -d : default"
    echo " -c : compile"
}

if [ $# -eq 0 ]; then    
    printHelp    
else    
    if [ "$1" = "-d" ]; then	
	./terzinag4_tessellation_ex init_vis.mac
    elif [ "$1" = "-c" ]; then	
	make -j
    else        
        printHelp            
    fi   
fi
