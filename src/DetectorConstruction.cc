//terzinag4_tessellation_ex
#include "DetectorConstruction.hh"
#include "meshToG4TessellatedSolid.hh"

//G4
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4VisAttributes.hh"
#include "G4TessellatedSolid.hh"
#include "G4TriangularFacet.hh"
#include "G4VFacet.hh"

//root
#include "TString.h"

//C, C++
#include <vector>
#include <iostream>

using namespace std;
using namespace CLHEP;


DetectorConstruction::DetectorConstruction(){
}

DetectorConstruction::~DetectorConstruction(){
}

G4VPhysicalVolume* DetectorConstruction::Construct(){
  //
  G4NistManager *nist_manager = G4NistManager::Instance();
  G4Material *air = nist_manager->FindOrBuildMaterial("G4_AIR");
  //
  G4VSolid *world_solid = new G4Box("world_solid", 500*cm, 500*cm, 500*cm);
  G4LogicalVolume *world_logical = new G4LogicalVolume(world_solid, air,"world_logical");
  G4VPhysicalVolume *world_physical = new G4PVPlacement(0, G4ThreeVector(), world_logical,
							"world_physical", 0, false, 0);
  //
  meshToG4TessellatedSolid *meshtosolid = new meshToG4TessellatedSolid();
  // 
  //mesh to tessellation
  G4TessellatedSolid* baffle_baseline_solid = new G4TessellatedSolid("baffle_baseline");
  G4TessellatedSolid* cover_blue_solid = new G4TessellatedSolid("cover_blue");
  G4TessellatedSolid* holder_unico_9_solid = new G4TessellatedSolid("holder_unico_9");
  G4TessellatedSolid* MLI_radiator_solid = new G4TessellatedSolid("MLI_radiator");
  G4TessellatedSolid* optical_bench_solid = new G4TessellatedSolid("optical_bench");
  G4TessellatedSolid* radiator_solid = new G4TessellatedSolid("radiator");
  G4TessellatedSolid* vanes_big_solid = new G4TessellatedSolid("vanes_big");
  G4TessellatedSolid* vanes_a_solid = new G4TessellatedSolid("vanes_a");
  G4TessellatedSolid* vanes_b_solid = new G4TessellatedSolid("vanes_b");
  G4TessellatedSolid* vanes_c_solid = new G4TessellatedSolid("vanes_c");
  G4TessellatedSolid* vanes_d_solid = new G4TessellatedSolid("vanes_d");
  //
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/baffle_baseline.stl",baffle_baseline_solid);  
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/cover_blue.stl",cover_blue_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/holder_unico_9.stl",holder_unico_9_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/MLI_radiator.stl",MLI_radiator_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/optical_bench.stl",optical_bench_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/radiator.stl",radiator_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/vanes_big.stl",vanes_big_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/vanes_a.stl",vanes_a_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/vanes_b.stl",vanes_b_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/vanes_c.stl",vanes_c_solid);
  meshtosolid->getTessellatedSolid("../config_v.00.00.02/vanes_d.stl",vanes_d_solid);
  //
  G4LogicalVolume *baffle_baseline_logical = new G4LogicalVolume(baffle_baseline_solid,air,"baffle_baseline_logical");
  G4LogicalVolume *cover_blue_logical = new G4LogicalVolume(cover_blue_solid,air,"cover_blue_logical");
  G4LogicalVolume *holder_unico_9_logical = new G4LogicalVolume(holder_unico_9_solid,air,"holder_unico_9_logical");
  G4LogicalVolume *MLI_radiator_logical = new G4LogicalVolume(MLI_radiator_solid,air,"MLI_radiator_logical");
  G4LogicalVolume *optical_bench_logical = new G4LogicalVolume(optical_bench_solid,air,"optical_bench_logical");
  G4LogicalVolume *radiator_logical = new G4LogicalVolume(radiator_solid,air,"radiator_logical");
  G4LogicalVolume *vanes_big_logical = new G4LogicalVolume(vanes_big_solid,air,"vanes_big_logical");
  G4LogicalVolume *vanes_a_logical = new G4LogicalVolume(vanes_a_solid,air,"vanes_a_logical");
  G4LogicalVolume *vanes_b_logical = new G4LogicalVolume(vanes_b_solid,air,"vanes_b_logical");
  G4LogicalVolume *vanes_c_logical = new G4LogicalVolume(vanes_c_solid,air,"vanes_c_logical");
  G4LogicalVolume *vanes_d_logical = new G4LogicalVolume(vanes_d_solid,air,"vanes_d_logical");
  //
  new G4PVPlacement(0, G4ThreeVector(), baffle_baseline_logical, "baffle_baseline_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), cover_blue_logical, "cover_blue_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), holder_unico_9_logical, "holder_unico_9_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), MLI_radiator_logical, "MLI_radiator_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), optical_bench_logical, "optical_bench_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), radiator_logical, "radiator_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), vanes_big_logical, "vanes_big_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), vanes_a_logical, "vanes_a_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), vanes_b_logical, "vanes_b_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), vanes_c_logical, "vanes_c_physical", world_logical, 0, false, 1);
  new G4PVPlacement(0, G4ThreeVector(), vanes_d_logical, "vanes_d_physical", world_logical, 0, false, 1);
  //
  return world_physical;
}
