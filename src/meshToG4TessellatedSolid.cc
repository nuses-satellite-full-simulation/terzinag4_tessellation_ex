//terzinag4_tessellation_ex
#include "meshToG4TessellatedSolid.hh"

//G4
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4TessellatedSolid.hh"
#include "G4TriangularFacet.hh"
#include "G4VFacet.hh"

//root
#include "TString.h"

//C, C++
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

meshToG4TessellatedSolid::meshToG4TessellatedSolid()
{
}

meshToG4TessellatedSolid::~meshToG4TessellatedSolid(){
}

bool meshToG4TessellatedSolid::getTessellatedSolid( TString fname, G4TessellatedSolid *tess){
  std::vector<G4ThreeVector> triangle_p1_v;
  std::vector<G4ThreeVector> triangle_p2_v;
  std::vector<G4ThreeVector> triangle_p3_v;
  if(read_stl_txt( fname, triangle_p1_v, triangle_p2_v, triangle_p3_v)){
    for (unsigned int i = 0; i < triangle_p1_v.size(); i++) {
      G4TriangularFacet* facet = new G4TriangularFacet(triangle_p1_v.at(i), triangle_p2_v.at(i), triangle_p3_v.at(i), ABSOLUTE);
      tess->AddFacet((G4VFacet*)facet);
    }
    tess->SetSolidClosed(true);
    return true;
  }   
  return false;
}

bool meshToG4TessellatedSolid::read_stl_txt(TString fname, std::vector<G4ThreeVector> &triangle_p1_v, std::vector<G4ThreeVector> &triangle_p2_v, std::vector<G4ThreeVector> &triangle_p3_v){
  std::string line;
  std::ifstream filein(fname.Data());
  double x,y,z;
  Int_t point_counter = 0;  
  if (filein.is_open()){
    while(filein>>line){
      if(line=="outer"){
	filein>>line;
	if(line=="loop"){
	  filein>>line;
	  if(line=="vertex"){
	    filein>>x>>y>>z;
	    G4ThreeVector v(x*CLHEP::mm,y*CLHEP::mm,z*CLHEP::mm);
	    triangle_p1_v.push_back(v);
	  }
	  filein>>line;
	  if(line=="vertex"){
	    filein>>x>>y>>z;
	    G4ThreeVector v(x*CLHEP::mm,y*CLHEP::mm,z*CLHEP::mm);
	    triangle_p2_v.push_back(v);
	  }
	  filein>>line;
	  if(line=="vertex"){
	    filein>>x>>y>>z;
	    G4ThreeVector v(x*CLHEP::mm,y*CLHEP::mm,z*CLHEP::mm);
	    triangle_p3_v.push_back(v);
	  }
	}
      }
    }
    filein.close();
    return true;
  }
  else{
    std::cout<< "Unable to open file : "<<fname<<std::endl; 
  }
  return false;
}
