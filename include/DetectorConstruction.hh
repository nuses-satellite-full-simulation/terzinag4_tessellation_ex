#ifndef DetectorConstruction_hh
#define DetectorConstruction_hh 1

//
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"

#include <string>
#include <vector>
#include <iostream>

class G4VSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;
class TString;

class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
  
  DetectorConstruction();
  ~DetectorConstruction();
  
  G4VPhysicalVolume* Construct();
  
private:
  
};

#endif

