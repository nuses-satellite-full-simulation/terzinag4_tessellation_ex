#ifndef meshToG4TessellatedSolid_hh
#define meshToG4TessellatedSolid_hh 1

//G4
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4TessellatedSolid;
class TString;

class meshToG4TessellatedSolid {
  
public:
  
  meshToG4TessellatedSolid();
  ~meshToG4TessellatedSolid();

  bool getTessellatedSolid( TString fname, G4TessellatedSolid *tess);
  
private:
  
  bool read_stl_txt(TString fname, std::vector<G4ThreeVector> &triangle_p1_v, std::vector<G4ThreeVector> &triangle_p2_v, std::vector<G4ThreeVector> &triangle_p3_v);
  
};

#endif
